﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace LinqPadExtensions
{
    public static class LinqPadExtensions
    {
        public static string Trololo(this object obj)
        {
            return "Trololo";
        }

        public static void WriteToFile(this IList<string> text, string filePath)
        {
            using (var file = new StreamWriter(filePath))
            {
                foreach (var line in text)
                {
                    file.WriteLine(line);
                }
            }
        }
    }
}
