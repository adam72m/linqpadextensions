﻿using System.Data.Entity;

namespace LinqPadExtensions
{
    public class RisiGoDbConfiguration : DbConfiguration
    {
        public RisiGoDbConfiguration()
        {
            SetDefaultConnectionFactory(new System.Data.Entity.Infrastructure.SqlConnectionFactory());
            SetProviderServices("System.Data.SqlClient", System.Data.Entity.SqlServer.SqlProviderServices.Instance);
        }
    }
}
