﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqPadExtensions
{
    public class ExcelFile
    {
        private List<List<string>> _rows = new List<List<string>>();

        public List<List<string>> Rows 
        {
            get { return _rows; }
            set { _rows = value; }
        }
    }
}
